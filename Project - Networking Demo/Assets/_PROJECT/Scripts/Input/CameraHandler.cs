using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using Mirror;
using UnityEngine;

public class CameraHandler : Singleton<CameraHandler> {

    [SerializeField] CameraMode cameraMode;

    [Header ("First Person")]
    [SerializeField] CinemachineVirtualCamera fpCamera;
    [SerializeField] GameObject fpRoot;

    [Header ("Third Person")]
    [SerializeField] CinemachineFreeLook tpCamera;
    [SerializeField] GameObject tpRoot;

    CinemachineVirtualCameraBase activeCamera;

    Transform followTarget;

    void Start () {
        if (NetworkManager.Instance.mode == NetworkManagerMode.ServerOnly) {
            gameObject.SetActive (false);
            return;
        }

        SetActiveCamera ();

        // Cursor.visible = false;
        // Cursor.lockState = CursorLockMode.Locked;
    }

    void SetActiveCamera () {
        fpRoot.SetActive (cameraMode == CameraMode.FirstPerson);
        tpRoot.SetActive (cameraMode == CameraMode.ThirdPerson);
        activeCamera = cameraMode == CameraMode.FirstPerson ? fpCamera : tpCamera;
    }

    public void SwitchCameras () {
        cameraMode = cameraMode == CameraMode.FirstPerson ? CameraMode.ThirdPerson : CameraMode.FirstPerson;
        SetActiveCamera ();
    }

    public void SetFollowTarget (Transform target) {
        followTarget = target;
    }

    void LateUpdate () {
        if (!followTarget) return;

        transform.position = followTarget.position;
        transform.forward = followTarget.forward;
    }

    public void LookVertical (float angle, float lookSpeed) {
        activeCamera.transform.Rotate (-Vector3.right * lookSpeed * angle); //Vertical look
        activeCamera.transform.localEulerAngles = new Vector3 (activeCamera.transform.localEulerAngles.x.ClampAngle (-90f, 90f), 0, 0);
    }

    enum CameraMode {
        FirstPerson,
        ThirdPerson
    }

}

public static class CameraExtensions {
    public static float ClampAngle (this float angle, float min, float max) {
        angle = Mathf.Repeat (angle, 360);
        min = Mathf.Repeat (min, 360);
        max = Mathf.Repeat (max, 360);
        bool inverse = false;
        var tmin = min;
        var tangle = angle;
        if (min > 180) {
            inverse = !inverse;
            tmin -= 180;
        }

        if (angle > 180) {
            inverse = !inverse;
            tangle -= 180;
        }

        var result = !inverse ? tangle > tmin : tangle < tmin;
        if (!result)
            angle = min;

        inverse = false;
        tangle = angle;
        var tmax = max;
        if (angle > 180) {
            inverse = !inverse;
            tangle -= 180;
        }

        if (max > 180) {
            inverse = !inverse;
            tmax -= 180;
        }

        result = !inverse ? tangle < tmax : tangle > tmax;
        if (!result)
            angle = max;
        return angle;
    }
}