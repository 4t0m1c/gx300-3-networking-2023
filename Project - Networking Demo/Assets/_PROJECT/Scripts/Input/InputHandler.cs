using UnityEngine;
using UnityEngine.Events;

public class InputHandler {

    public UnityEvent<Vector2> OnMovePerformed = new UnityEvent<Vector2> ();
    public UnityEvent OnMoveCanceled = new UnityEvent ();
    public UnityEvent<Vector2> OnLookPerformed = new UnityEvent<Vector2> ();
    public UnityEvent<bool> OnSprintPerformed = new UnityEvent<bool> ();

    public UnityEvent OnJumpPerformed = new UnityEvent ();
    public UnityEvent OnFirePerformed = new UnityEvent ();
    public UnityEvent OnSwitchCameras = new UnityEvent ();

    public Vector2 MoveVector;
    public Vector2 LookVector;

    PlayerInput playerInput;

    public InputHandler () {
        playerInput = new PlayerInput ();
        RegisterInputs ();
    }

    public void Enable () {
        playerInput.Enable ();
    }

    public void Disable () {
        playerInput.Disable ();
    }

    public void Dispose () {
        playerInput.Dispose ();
    }

    void RegisterInputs () {
        playerInput.Player.Move.performed += x => {
            MoveVector = x.ReadValue<Vector2> ();
            OnMovePerformed.Invoke (MoveVector);
        };

        playerInput.Player.Move.canceled += x => {
            MoveVector = Vector2.zero;
            OnMoveCanceled.Invoke ();
        };

        playerInput.Player.Look.performed += x => {
            LookVector = x.ReadValue<Vector2> ();
            OnLookPerformed.Invoke (LookVector);
        };

        playerInput.Player.Sprint.performed += x => {
            OnSprintPerformed.Invoke (true);
        };

        playerInput.Player.Sprint.canceled += x => {
            OnSprintPerformed.Invoke (false);
        };

        playerInput.Player.Jump.performed += x => {
            OnJumpPerformed.Invoke ();
        };

        playerInput.Player.Fire.performed += x => {
            OnFirePerformed.Invoke ();
        };
        
        playerInput.Player.SwitchCameras.performed += x => {
            OnSwitchCameras.Invoke ();
        };
    }

}