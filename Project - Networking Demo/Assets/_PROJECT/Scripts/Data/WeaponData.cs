using UnityEngine;

[CreateAssetMenu (fileName = "Weapon_", menuName = "Data/Weapon Data", order = 0)]
public class WeaponData : ScriptableObject {

    public string weaponName;
    public GameObject weaponModelPrefab;

}