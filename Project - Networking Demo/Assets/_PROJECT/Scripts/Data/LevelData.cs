using Mirror;
using UnityEngine;

[CreateAssetMenu (fileName = "Level_", menuName = "Data/Level Data", order = 0)]
public class LevelData : ScriptableObject {
    [Scene] public string sceneName;
}