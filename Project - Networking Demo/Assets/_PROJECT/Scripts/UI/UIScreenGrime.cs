using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIScreenGrime : MonoBehaviour {

    [SerializeField] Image grimeImage;
    [SerializeField] float maxValue = 100;

    public void SetOpacity (float opacity) {
        grimeImage.DOFade (1 - (opacity / maxValue), 0.3f);
    }

}