using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UILobbyReady : MonoBehaviour {

    [SerializeField] bool ready = false;
    [SerializeField] TMP_Text readyText;
    
    void OnEnable () {
        LobbyManager.OnGameStateUpdated.AddListener (GameStateUpdated);
    }

    void OnDisable () {
        LobbyManager.OnGameStateUpdated.RemoveListener (GameStateUpdated);
    }
    
    void GameStateUpdated (GameState gameState) {
        if (gameState != GameState.Waiting && ready) {
            ready = !ready;
            readyText.text = ready ? "Unready" : "Ready";
        }
    }

    public void ReadyToggle () {
        ready = !ready;
        readyText.text = ready ? "Unready" : "Ready";

        Player.Instance.SetReady (ready);
    }
    
}