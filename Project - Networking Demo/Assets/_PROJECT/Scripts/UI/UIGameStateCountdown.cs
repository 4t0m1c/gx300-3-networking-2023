using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIGameStateCountdown : MonoBehaviour {

    [SerializeField] Canvas countDownCanvas;
    [SerializeField] TMP_Text countdownText;
    [SerializeField] TMP_Text gamestateText;

    void OnEnable () {
        LobbyManager.OnReadyCountdownUpdated.AddListener (ReadyCountdownUpdated);
        LobbyManager.OnGameStateUpdated.AddListener (GameStateUpdated);
    }

    void OnDisable () {
        LobbyManager.OnReadyCountdownUpdated.RemoveListener (ReadyCountdownUpdated);
        LobbyManager.OnGameStateUpdated.RemoveListener (GameStateUpdated);
    }

    void ReadyCountdownUpdated (int count) {
        if (countdownText) {
            countDownCanvas.enabled = count > 0;
            countdownText.text = count.ToString ();
        }
    }

    void GameStateUpdated (GameState gameState) {
        if (gamestateText) {
            gamestateText.text = gameState.ToString ();
        }
    }
}