using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

public class SpawnPointManager : MonoBehaviour {

    public static SpawnPointManager Instance;

    [SerializeField] List<Transform> spawnPoints = new List<Transform> ();

    void Awake () {
        Instance = this;
    }

    public Transform GetSpawnPoint () {
        Transform _spawnPoint = spawnPoints[Random.Range (0, spawnPoints.Count)];
        spawnPoints.Remove (_spawnPoint);
        return _spawnPoint;
    }
    
    #if UNITY_EDITOR
    [Button]
    void CollectChildTransforms () {
        for (int i = 0; i < transform.childCount; i++) {
            spawnPoints.Add (transform.GetChild (i));
        }

        EditorUtility.SetDirty (this);
    }
    #endif

}