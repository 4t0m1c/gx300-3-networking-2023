using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;

public class NetworkManager : Mirror.NetworkManager {

    public static NetworkManager Instance;

    [SerializeField] GameObject lobbyManagerPrefab;
    public bool loadingLevel = false;

    int scenesLoaded = 0;

    public override void Start () {
        base.Start ();
        Instance = this;

#if UNITY_EDITOR
        networkAddress = "localhost";
#endif
    }

    public override void OnStartServer () {
        GameObject _lobbyManager = Instantiate (lobbyManagerPrefab);
        NetworkServer.Spawn (_lobbyManager);
    }

    public void LoadScene (LevelData levelData) {
        loadingLevel = true;
        ServerChangeScene (levelData.sceneName);
    }

    public override void OnServerSceneChanged (string sceneName) {
        base.OnServerSceneChanged (sceneName);
        Debug.Log ($"Server Scene Loaded");
    }

    public override void OnClientSceneChanged () {
        base.OnClientSceneChanged ();
        Player.Instance?.ClientSceneLoaded ();
        autoCreatePlayer = false;
    }

    public void ClientSceneLoaded () {
        scenesLoaded++;
        if (scenesLoaded == NetworkServer.connections.Count) {
            Debug.Log ($"Client Scene Loaded");
            loadingLevel = false;
            scenesLoaded = 0;
        }
    }
}