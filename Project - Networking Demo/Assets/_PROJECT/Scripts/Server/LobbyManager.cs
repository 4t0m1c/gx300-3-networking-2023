using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Mirror;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class LobbyManager : NetworkBehaviour {

    public static LobbyManager Instance;

    public static UnityEvent<int> OnReadyCountdownUpdated = new UnityEvent<int> ();
    public static UnityEvent<GameState> OnGameStateUpdated = new UnityEvent<GameState> ();

    [Title ("Round Times")]
    [SerializeField] float timeWarmup = 5;
    [SerializeField] float timeGameplay = 30;
    [SerializeField] float timeSummary = 5;

    [Title ("Game Settings")]
    [SerializeField] int minPlayers = 2;
    [SerializeField] int maxPlayers = 2;
    [SerializeField] float readyCountdown = 5;
    [SerializeField] LevelData lobbyLevel;
    [SerializeField] List<LevelData> levels = new List<LevelData> ();

    [Title ("Debug")]
    [SerializeField] List<Player> playersConnected = new List<Player> ();
    [SerializeField] List<Player> playersReady = new List<Player> ();
    [SyncVar (hook = nameof (GameStateUpdated))]
    public GameState gameState;
    [SerializeField] bool matchReady = false;

    void Awake () {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (gameObject);
        } else {
            DestroyImmediate (gameObject);
        }
    }

    void Start () {
        SetGameState (GameState.Waiting);
    }

    /*
        CONNECTION 
    */

    public void AddPlayer (Player player) {
        if (!playersConnected.Contains (player)) {
            playersConnected.Add (player);
            Debug.Log ($"Player Connected. [{playersConnected.Count}]");
        }
    }

    public void RemovePlayer (Player player) {
        if (playersConnected.Contains (player)) {
            playersConnected.Remove (player);
            Debug.Log ($"Player Disconnected. [{playersConnected.Count}]");
        }
    }

    /*
        READY
    */

    public void PlayerReady (Player player, bool ready) {
        if (ready) {
            if (!playersReady.Contains (player)) {
                playersReady.Add (player);
                Debug.Log ($"Player ready: [{playersReady.Count}/{minPlayers}]");
            }
        } else {
            if (playersReady.Contains (player)) {
                playersReady.Remove (player);
                Debug.Log ($"Player not ready: [{playersReady.Count}/{minPlayers}]");
            }
        }

        CheckMatchReady ();
    }

    void CheckMatchReady () {
        if (gameState == GameState.Waiting) {
            if (playersReady.Count >= minPlayers) {
                Debug.Log ($"Match Starting");

                if (!matchReady) {
                    StartCoroutine (MatchReadyCountdown ());
                }
            }
        }
    }

    IEnumerator MatchReadyCountdown () {
        matchReady = true;
        float countDownTime = readyCountdown;
        int currentSeconds = (int) readyCountdown;
        RpcUpdateCountdownTimer (currentSeconds);

        while (countDownTime > 0) {
            countDownTime -= Time.deltaTime;

            if (currentSeconds - countDownTime >= 1) {
                currentSeconds--;
                RpcUpdateCountdownTimer (currentSeconds);
            }

            if (playersReady.Count < minPlayers) {
                matchReady = false;
                break;
            }

            yield return null;
        }

        RpcUpdateCountdownTimer (0);

        if (playersReady.Count >= minPlayers) {
            SetGameState (GameState.Warmup);
        }
    }

    [ClientRpc]
    void RpcUpdateCountdownTimer (int time) {
        OnReadyCountdownUpdated.Invoke (time);
    }

    /*
        GAMESTATE 
    */

    [Server]
    void SetGameState (GameState gameState) {
        this.gameState = gameState; //SYNCVAR

        switch (gameState) {
            case GameState.Waiting:
                OnGameStateUpdated.Invoke (gameState);
                matchReady = false;
                break;
            case GameState.Warmup:
                StartCoroutine (State_Warmup ());
                break;
            case GameState.Gameplay:
                OnGameStateUpdated.Invoke (gameState);
                StartCoroutine (State_Gameplay ());
                break;
            case GameState.Summary:
                OnGameStateUpdated.Invoke (gameState);
                StartCoroutine (State_Summary ());
                break;
        }
    }

    [Client]
    void GameStateUpdated (GameState oldGameState, GameState newGameState) {
        OnGameStateUpdated.Invoke (newGameState);
    }

    IEnumerator State_Warmup () {
        //Select a random level and load it
        LevelData levelSelected = levels[Random.Range (0, levels.Count)];
        NetworkManager.Instance.LoadScene (levelSelected);

        while (NetworkManager.Instance.loadingLevel) {
            yield return null;
        }

        OnGameStateUpdated.Invoke (gameState);

        float timer = timeWarmup;
        int lastSeconds = (int) timer;
        RpcUpdateCountdownTimer (lastSeconds);

        while (timer > 0 && playersReady.Count >= minPlayers) {
            timer = Timer (timer, ref lastSeconds);

            yield return null;
        }

        RpcUpdateCountdownTimer (0);
        SetGameState (GameState.Gameplay);
    }

    IEnumerator State_Gameplay () {
        float timer = timeGameplay;
        int lastSeconds = (int) timer;
        RpcUpdateCountdownTimer (lastSeconds);

        while (timer > 0 && playersReady.Count >= minPlayers) {
            timer = Timer (timer, ref lastSeconds);

            yield return null;
        }

        RpcUpdateCountdownTimer (0);
        SetGameState (GameState.Summary);
    }

    IEnumerator State_Summary () {
        float timer = timeSummary;
        int lastSeconds = (int) timer;
        RpcUpdateCountdownTimer (lastSeconds);

        while (timer > 0 && playersReady.Count >= minPlayers) {
            timer = Timer (timer, ref lastSeconds);

            yield return null;
        }

        RpcUpdateCountdownTimer (0);

        List<Player> _players = new List<Player> (playersReady);
        foreach (Player _player in _players) {
            PlayerReady (_player, false);
        }
        
        NetworkManager.Instance.LoadScene (lobbyLevel);

        while (NetworkManager.Instance.loadingLevel) {
            yield return null;
        }

        SetGameState (GameState.Waiting);
    }

    float Timer (float timer, ref int lastSeconds) {
        timer -= Time.deltaTime;
        if (lastSeconds - timer >= 1) {
            lastSeconds--;
            RpcUpdateCountdownTimer (lastSeconds);
        }

        return timer;
    }

}

public enum GameState {
    Waiting,
    Warmup,
    Gameplay,
    Summary
}