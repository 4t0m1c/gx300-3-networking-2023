using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

public class Bullet : NetworkBehaviour {
    UnityAction<PlayerCharacter, DamageBundle> OnHitPlayerCharacter;
    DamageBundle damageBundle;
    
    public void RegisterBulletHitListener (DamageBundle damageBundle, UnityAction<PlayerCharacter, DamageBundle> OnHitPlayerCharacter) {
        this.OnHitPlayerCharacter = OnHitPlayerCharacter;
        this.damageBundle = damageBundle;
    }

    void OnCollisionEnter (Collision collision) {
        if (isServer) {
            if (collision.collider.attachedRigidbody && collision.collider.attachedRigidbody.TryGetComponent<PlayerCharacter> (out PlayerCharacter _playerCharacter)) {
                OnHitPlayerCharacter?.Invoke (_playerCharacter, damageBundle);
            }
        }
    }

}