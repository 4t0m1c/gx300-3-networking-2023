using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using UnityEngine;
using Random = UnityEngine.Random;

public class Pickup : NetworkBehaviour {

    [SerializeField] List<WeaponData> weapons = new List<WeaponData> ();
    [SerializeField] Transform weaponHolder;
    [SyncVar, SerializeField] int selectedWeaponIndex = 0;
    GameObject weaponModel;

    public override void OnStartServer () {
        selectedWeaponIndex = Random.Range (0, weapons.Count);
    }

    public override void OnStartClient () {
        WeaponData _selectedWeapon = weapons[selectedWeaponIndex];
        weaponModel = Instantiate (_selectedWeapon.weaponModelPrefab, weaponHolder);
    }

    void OnTriggerEnter (Collider other) {
        if (!isServer) return;
        if (selectedWeaponIndex < 0) return;

        if (other.attachedRigidbody && other.attachedRigidbody.TryGetComponent (out PlayerCharacter _playerCharacter)) {
            _playerCharacter.ServerPickup (weapons[selectedWeaponIndex]);
            RpcClientPickup (_playerCharacter, selectedWeaponIndex);
            selectedWeaponIndex = -1;
        }
    }

    [ClientRpc]
    void RpcClientPickup (PlayerCharacter playerCharacter, int index) {
        playerCharacter.ClientPickup (weapons[index]);
        Destroy (weaponModel);
    }

}