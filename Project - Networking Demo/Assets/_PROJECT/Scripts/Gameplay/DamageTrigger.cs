using System;
using System.Collections;
using System.Collections.Generic;
using Mirror;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.Events;

public class DamageTrigger : NetworkBehaviour {

    [Title ("Settings")]
    [SerializeField] float maxDamage = 10;
    [SerializeField] float duration = 3;
    [SerializeField] float cooldown = 3;
    [SerializeField] int damageTicks = 3;
    [SerializeField] AnimationCurve damageOverTimeNormalised;

    [Title ("Output")]
    [SerializeField] UnityEvent OnTriggered = new UnityEvent ();

    [Title ("Debug")]
    [SerializeField, ReadOnly] Dictionary<PlayerCharacter, int> playerCharacters = new Dictionary<PlayerCharacter, int> ();
    [SerializeField, ReadOnly] bool triggered = false;

    void OnTriggerEnter (Collider other) {
        if (isServer) {
            //Detect PlayerCharacter
            if (other.attachedRigidbody && other.attachedRigidbody.TryGetComponent (out PlayerCharacter _playerCharacter)) {
                if (!playerCharacters.ContainsKey (_playerCharacter)) playerCharacters.Add (_playerCharacter, 1);
                else playerCharacters[_playerCharacter] += 1;

                Debug.Log ($"Player {_playerCharacter.netId} entered damageTrigger");
                if (!triggered) {
                    triggered = true;
                    RpcTriggered ();
                    StartCoroutine (DamageOverTime ());
                }
            }
        }
    }

    void OnTriggerExit (Collider other) {
        if (isServer) {
            //Detect PlayerCharacter
            if (other.attachedRigidbody && other.attachedRigidbody.TryGetComponent (out PlayerCharacter _playerCharacter)) {
                Debug.Log ($"Player {_playerCharacter.netId} left damageTrigger");
                if (playerCharacters.ContainsKey (_playerCharacter)) {
                    if (playerCharacters[_playerCharacter] > 1) playerCharacters[_playerCharacter] -= 1;
                    else playerCharacters.Remove (_playerCharacter);
                }
            }
        }
    }

    [Server]
    IEnumerator DamageOverTime () {
        float _duration = duration;
        float _damageInterval = _duration / damageTicks;
        float _currentInterval = 0;
        Debug.Log ($"Damage Trigger Activated");

        while (_duration >= 0) {
            _duration -= Time.deltaTime;
            _currentInterval -= Time.deltaTime;

            if (_currentInterval <= 0) {
                DamageBundle _damageBundle = new (damageOverTimeNormalised.Evaluate (_duration / duration) * maxDamage, 1);
                playerCharacters.ForEach (x => x.Key.DoDamage (_damageBundle));
                _currentInterval = _damageInterval;
            }

            yield return null;
        }

        yield return new WaitForSeconds (cooldown);
        triggered = false;
    }

    [ClientRpc]
    void RpcTriggered () {
        OnTriggered?.Invoke ();
    }

}