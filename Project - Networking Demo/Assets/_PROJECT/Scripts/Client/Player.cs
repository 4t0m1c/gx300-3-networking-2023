using System;
using UnityEngine;
using Mirror;
using UnityEditor;
using UnityEngine.Events;

public class Player : NetworkBehaviour {

    public static Player Instance;

    bool ready = false;

    [SerializeField] GameObject playerCharacterPrefab;

    PlayerCharacter playerCharacter;

    public override void OnStartClient () {
        DontDestroyOnLoad (gameObject);

        Debug.Log ($"{netId} OnStartClient");
    }

    public override void OnStartServer () {
        DontDestroyOnLoad (gameObject);

        LobbyManager.OnGameStateUpdated.AddListener (GameStateUpdated);

        Debug.Log ($"{netId} OnStartServer");
        LobbyManager.Instance.AddPlayer (this);
    }

    public override void OnStopServer () {
        LobbyManager.OnGameStateUpdated.RemoveListener (GameStateUpdated);

        Debug.Log ($"{netId} OnStopServer");
        LobbyManager.Instance.RemovePlayer (this);
    }

    public override void OnStartLocalPlayer () {
        LobbyManager.OnGameStateUpdated.AddListener (GameStateUpdated);

        Debug.Log ($"{netId} OnStartLocalPlayer");
        Instance = this;
    }

    public override void OnStopLocalPlayer () {
        LobbyManager.OnGameStateUpdated.RemoveListener (GameStateUpdated);

        Debug.Log ($"{netId} OnStopLocalPlayer");
    }

    /*
        READY
    */

    [Client]
    public void SetReady (bool ready) {
        this.ready = ready;
        CmdSetReady (ready);
    }

    [Command]
    void CmdSetReady (bool ready) {
        this.ready = ready;
        LobbyManager.Instance.PlayerReady (this, ready);
    }

    /*
        GAMELOOP
    */

    void GameStateUpdated (GameState gameState) {
        if (isServer) {
            if (!ready) return;
            
            if (gameState == GameState.Warmup) {
                //Spawn Player Character
                playerCharacter = Instantiate (playerCharacterPrefab).GetComponent<PlayerCharacter>();
                NetworkServer.ReplacePlayerForConnection (connectionToClient, playerCharacter.gameObject, true);
            }
            
            if (gameState == GameState.Summary) {
                //Destroy Player Character
                NetworkServer.ReplacePlayerForConnection (connectionToClient, gameObject, false);
                NetworkServer.Destroy (playerCharacter.gameObject);
                playerCharacter = null;
            }
        }

        if (isClient) {
            // Do something else
        }
    }

    [Client]
    public void ClientSceneLoaded () {
        CmdClientSceneLoaded ();
    }

    [Command]
    void CmdClientSceneLoaded () {
        NetworkManager.Instance.ClientSceneLoaded ();
    }
}