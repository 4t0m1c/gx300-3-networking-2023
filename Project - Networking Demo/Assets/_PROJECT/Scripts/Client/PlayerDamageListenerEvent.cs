using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerDamageListenerEvent : MonoBehaviour {

    [SerializeField] UnityEvent<float> OnHealthChanged = new UnityEvent<float> ();
    [SerializeField] UnityEvent<float> OnDamageChanged = new UnityEvent<float> ();
    [SerializeField] bool localPlayerOnly = false;

    void OnEnable () {
        PlayerCharacter.OnDamaged.AddListener (Damaged);
    }

    void OnDisable () {
        PlayerCharacter.OnDamaged.RemoveListener (Damaged);
    }

    void PlayerCharacterCreated () {
        Debug.Log ($"PlayerCharacter spawned: {PlayerCharacter.Instance}");
    }

    void Damaged (PlayerCharacter playerCharacter, float health, float damageChange) {
        if (!localPlayerOnly || (localPlayerOnly && playerCharacter == PlayerCharacter.Instance)) {
            OnHealthChanged?.Invoke (health);
            OnDamageChanged?.Invoke (damageChange);
        }
    }
}