using System.Collections;
using Mirror;
using UnityEngine;
using UnityEngine.Events;

public class PlayerCharacter : NetworkBehaviour {

    public static PlayerCharacter Instance;

    public static UnityEvent<PlayerCharacter, float, float> OnDamaged = new UnityEvent<PlayerCharacter, float, float> ();

    [Header ("References")]
    [SerializeField] new Rigidbody rigidbody;
    [SerializeField] Animator animator;
    [SerializeField] Transform weaponHolder;

    [Header ("Input")]
    [SerializeField] float lookSpeed = 1;
    [SerializeField] float moveSpeed = 1;
    [SerializeField] float jumpForce = 1;
    [SerializeField] float jumpTimeout = 0.5f;
    [SerializeField] LayerMask groundMask;

    [Header ("Player Stats")]
    [SerializeField] float health = 100;
    [SerializeField] WeaponData weapon;

    InputHandler inputHandler;
    bool isSprinting = false;
    float jumpTimeoutCurrent;
    bool isGrounded = true;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] Transform bulletHolder;

    static readonly int X = Animator.StringToHash ("X");
    static readonly int Y = Animator.StringToHash ("Y");
    static readonly int Moving = Animator.StringToHash ("Moving");

    public override void OnStartLocalPlayer () {
        Instance = this;
        inputHandler = new InputHandler ();
        inputHandler.Enable ();
        RegisterInputs ();
    }

    public override void OnStopLocalPlayer () {
        inputHandler.Disable ();
        inputHandler.Dispose ();
        DeregisterInputs ();
    }

    public override void OnStartServer () {
        if (SpawnPointManager.Instance) SetSpawnPoint (SpawnPointManager.Instance.GetSpawnPoint ());
    }

    void RegisterInputs () {
        inputHandler.OnSprintPerformed.AddListener (OnSprint);
        inputHandler.OnSwitchCameras.AddListener (OnSwitchCameras);
        StartCoroutine (WaitForCamera ());
    }

    void DeregisterInputs () {
        inputHandler.OnSprintPerformed.RemoveListener (OnSprint);
        inputHandler.OnSwitchCameras.RemoveListener (OnSwitchCameras);
    }

    IEnumerator WaitForCamera () {
        while (!CameraHandler.Instance) {
            yield return null;
        }

        CameraHandler.Instance.SetFollowTarget (transform);
    }

    void Update () {
        if (!isLocalPlayer) return;

        OnLook ();
        OnMove ();
        GroundCheck ();
    }

    [Server]
    void SetSpawnPoint (Transform spawnPoint) {
        transform.position = spawnPoint.position;
        transform.forward = spawnPoint.forward;
    }

    /*
        INPUTS 
    */

    void OnSwitchCameras () {
        CameraHandler.Instance.SwitchCameras ();
    }

    void OnMove () {
        Vector3 localVelocity = transform.TransformDirection (new Vector3 (inputHandler.MoveVector.x, 0, inputHandler.MoveVector.y) * (moveSpeed * (isSprinting ? 1.5f : 1f)));
        rigidbody.velocity = new Vector3 (localVelocity.x, rigidbody.velocity.y, localVelocity.z);
        animator.SetBool (Moving, inputHandler.MoveVector != Vector2.zero);
        animator.SetFloat (X, inputHandler.MoveVector.x);
        animator.SetFloat (Y, inputHandler.MoveVector.y);
    }

    void OnLook () {
        if (!CameraHandler.Instance) return;

        RotateHorizontal (inputHandler.LookVector.x);
        RotateVertical (inputHandler.LookVector.y);

        void RotateHorizontal (float angle) {
            transform.Rotate (Vector3.up * (lookSpeed * angle)); //Horizontal look
        }

        void RotateVertical (float angle) {
            CameraHandler.Instance.LookVertical (angle, lookSpeed);
        }
    }

    void OnSprint (bool sprint) {
        Debug.Log ($"Sprint Toggled: {sprint}");
        isSprinting = enabled;
    }

    void GroundCheck () {
        if (jumpTimeoutCurrent > 0) {
            jumpTimeoutCurrent -= Time.deltaTime;
        }

        RaycastHit hit;
        bool isHit = Physics.SphereCast (transform.position + Vector3.up, 0.5f, Vector3.down, out hit, 1f, groundMask, QueryTriggerInteraction.Ignore);

        if (isHit) {
            isGrounded = true;
            rigidbody.AddForce (Vector3.down * (jumpForce / 2));
        } else {
            isGrounded = false;
            if (rigidbody.velocity.y < 0) {
                rigidbody.AddForce (Vector3.down * (jumpForce / 2));
            }
        }
    }

    void OnJump () {
        if (isGrounded && jumpTimeoutCurrent <= 0) {
            jumpTimeoutCurrent = jumpTimeout;
            rigidbody.AddForce (Vector3.up * jumpForce, ForceMode.Impulse);
            isGrounded = false;
        }
    }

    void OnShoot () {
        //Local client
        //Play a sound
        //UnityEvent for UI to update
        //Animation event

        CmdOnShoot ();
    }

    [Command]
    void CmdOnShoot () {
        //Is there ammo?
        //Is the gun on cooldown?
        //Health check?
        //Any sort of validation?

        Bullet bullet = Instantiate (bulletPrefab, bulletHolder.position, bulletHolder.rotation).GetComponent<Bullet> ();
        bullet.RegisterBulletHitListener (new DamageBundle (10, 1), HitPlayerCharacter);
    }

    /*
        DAMAGE 
    */

    [Server]
    void HitPlayerCharacter (PlayerCharacter hitPlayerCharacter, DamageBundle damageBundle) {
        Debug.Log ($"Hit PlayerCharacter {hitPlayerCharacter.netId}");
        //Hit validation
        hitPlayerCharacter.DoDamage (damageBundle);
    }

    [Server]
    public void DoDamage (DamageBundle damageBundle) {
        //Health subtraction
        //Armour handling
        float _damageChange = damageBundle.GetDamage ();
        health -= _damageChange;

        RpcDoDamage (health, _damageChange);
    }

    [ClientRpc]
    void RpcDoDamage (float newHealth, float damageChange) {
        health = newHealth;
        Debug.Log ($"Damage taken: {damageChange} | Health: {newHealth}");
        //UnityEvent<float> for damageChange
        //UnityEvent<float> for health

        OnDamaged?.Invoke (this, newHealth, damageChange);
    }

    /*
        PICKUPS 
    */

    [Server]
    public void ServerPickup (WeaponData weapon) {
        Debug.Log ($"{netId} playerCharacter picked up Weapon: {weapon.weaponName}");
        this.weapon = weapon;
        //Calculate damage and use stats
    }

    [Client]
    public void ClientPickup (WeaponData weapon) {
        Debug.Log ($"Picked up Weapon: {weapon.weaponName}");
        this.weapon = weapon;
        //Display the model
        if (weaponHolder.childCount > 0) {
            for (int i = weaponHolder.childCount - 1; i >= 0; i--) {
                Destroy (weaponHolder.GetChild (i).gameObject);
            }
        }

        GameObject weaponModel = Instantiate (weapon.weaponModelPrefab);
        weaponModel.transform.parent = weaponHolder;
        weaponModel.transform.rotation = weaponHolder.rotation;
        weaponModel.transform.localPosition = Vector3.zero;
        
        Debug.Log ($"Spawned weapon prefab", weaponModel);
    }
}

[System.Serializable]
public class DamageBundle {
    public float weaponDamage;
    public float bulletDamageMultiplier;

    public DamageBundle (float weaponDamage, float bulletDamageMultiplier) {
        this.weaponDamage = weaponDamage;
        this.bulletDamageMultiplier = bulletDamageMultiplier;
    }

    public DamageBundle () {
        weaponDamage = 10;
        bulletDamageMultiplier = 1;
    }

    public float GetDamage () {
        return weaponDamage * bulletDamageMultiplier;
    }
}